import com.sun.xml.internal.ws.api.model.CheckedException;

import java.util.*;
import java.io.File;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created by admin on 04.07.2017.
 */
public class Chapter1 {
    private static String getFileExtension(String mystr) {
        int index = mystr.indexOf('.');
        return index == -1? null : mystr.substring(index);
    }

    private static void ex1_3() {
        System.out.println("== 1.3 =================================");
        File root = new File(".\\.idea");
        File[] files = root.listFiles((f)-> {
            return f.isFile()
                    && ".xml".compareToIgnoreCase(getFileExtension(f.getName())) == 0;
        });
        for (File f:files) {
            System.out.println(f.getName());
        }
    }
    private static void ex1_2() {
        System.out.println("== 1.2 =================================");
        File root = new File(".");
        File[] files = root.listFiles(File::isDirectory);
        for (File f:files) {
            System.out.println(f.getName());
        }
    }
    private static void ex1_4() {
        System.out.println("== 1.4 =================================");
        File root = new File("d:\\temp");
        File[] files = root.listFiles();
        Arrays.sort(files, (first,second)->{
            if (first.isDirectory() ^ second.isDirectory()) {
                return first.isDirectory() ? -1 : 1;
            }
            return first.getName().compareTo(second.getName());
        });
        for (File f:files) {
            System.out.println((f.isDirectory() ? "[DIR]  " : "[FILE] ") + f.getName());
        }
    }
    private static Runnable uncheck(RunnableEx runner) {
        return ()-> {
            try {
                runner.run();
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        };
    }
    private static void ex1_6() {
        new Thread(uncheck(()->{
            System.out.println("zzz");
            Thread.sleep(5000);
        })).start();
    }
    static Runnable andThen(Runnable first, Runnable second) {
        return () -> {first.run(); second.run();};
    }

    private static void ex1_9() {
        andThen(
                ()-> System.out.println("first"),
                ()-> System.out.println("second")
        ).run();
    }
    private static void ex1_10() {
        String[] names = {"Peter", "Paul", "Mary"};
        List<Runnable> runners = new ArrayList<>();
        for (String name: names) {
            runners.add(()->{
                System.out.println(name);
            });
        }
        for (Runnable r : runners) {
            r.run();
        }
//        runners.clear();
//        int i;
//        for (i = 0; i < names.length; i++) {
//            runners.add(()->{
//                System.out.println(names[i]);
//            });
//        }
//        for (Runnable r : runners) {
//            r.run();
//        }
        // Error:(99, 42) java: local variables referenced from a lambda expression must be final or effectively final
    }

    public static void main (String[] params) {
//        ex1_2();
//        ex1_3();
//        ex1_4();
//        ex1_6();
//        ex1_9();
//        ex1_10();
        ex1_12();
    }

    private static void ex1_12() {
        String[] names = {"Peter", "Paul", "Mary", "john"};

        Collection2<String> list = new ArrayList2<String>();
        list.addAll(Arrays.asList(names));
        
        list.forEachIf(
                System.out::println,
                Chapter1::firstLetterIsUpper
        );
    }

    private static boolean firstLetterIsUpper(String s) {
        return s.charAt(0) == Character.toUpperCase(s.charAt(0));
    }

}

interface Collection2<T> extends Collection<T> {
    default void forEachIf(Consumer<T> action, Predicate<T> filter) {
        this.forEach((item)->{
            if (filter.test(item)) action.accept(item); 
        });
    }
} 

class ArrayList2<T> extends ArrayList<T> implements Collection2<T> {}; 

@FunctionalInterface
interface RunnableEx {
    public abstract void run() throws Throwable;    
}