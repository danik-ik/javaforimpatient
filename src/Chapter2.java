import com.sun.istack.internal.NotNull;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.*;

/**
 * Created by danik_ik on 06.07.2017.
 */
public class Chapter2 {
    public static void main(String[] args){
//        Exercise_2_1();
//        Exercise_2_2();
//        Exercise_2_3();
//        Exercise_2_4();
//        Exercise_2_5();
//        Exercise_2_6();
//        Exercise_2_7();
//        Exercise_2_8();
        // TODO: 09.07.2017 Write tests?
        Exercise_2_9();
    }

    private static void Exercise_2_9() {
        printHeader();

        Integer[][] a = {{1,2,3,4,5,6},{11,12,13,14},{21,22,23,24,25,26,27,28}};

        // I don't want to use reduce() here :(
        Stream<ArrayList<Integer>> s =
                Arrays.stream(a).map((ai)->new ArrayList<>(Arrays.asList(ai)));
        ArrayList<Integer> result = s
                .reduce(new ArrayList<Integer>(), (t, u) -> {t.addAll(u); return t;});
        result.forEach(System.out::println);
        System.out.println();

        // Yes, I really don't want to use reduce() for this task :((
        s = Arrays.stream(a).map((ai)->new ArrayList<>(Arrays.asList(ai)));
        Optional<ArrayList<Integer>> result2 = s.reduce((t, u) -> {t.addAll(u); return t;});
        result2.ifPresent(integers -> integers.forEach(System.out::println));
        System.out.println();

        // Bad sample for reduce. I don't understand why I must use third form of reduce.
        // Result is the same type with source items. WHY this?!!
        s = Arrays.stream(a).map((ai)->new ArrayList<>(Arrays.asList(ai)));
        result = s.reduce(
                new ArrayList<>(),
                (t, u) -> {t.addAll(u); return t;},
                (t, u) -> {t.addAll(u); return t;}
        );
        result.forEach(System.out::println);
        System.out.println();

        // It must be something like this:
        s = Arrays.stream(a).map((ai)->new ArrayList<>(Arrays.asList(ai)));
        result = s
                .flatMap(Collection::stream)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        result.forEach(System.out::println);
        System.out.println();
    }

    /**
     * This iteraror gets one value from each source, then proceeds to the next source in a circle
     * Why it is not a Splitrator: 
     * 1) result must be ordered, but
     * 2) I can't to split all sources at one position, therefore
     * 3) "If you don't want to write trySplit(), don't write your own Spliterator" ((c) somebody from internet)
     * @param <T> type of all source streams
     */
    private static class AlternatingIterator<T> implements Iterator<T> {
        private static class Item<T>{
            Item (Stream<T> s) {this.partialIterator = s.iterator();};
            Iterator<T> partialIterator;
            Item<T> next;
        }
        private Item<T> current, last, prev;
        private void swapIterators() {
            current = current.next;
        }

        @SafeVarargs
        AlternatingIterator(Stream<T>... sources){
            Objects.requireNonNull(sources);
            if (sources.length == 0)
                throw new IllegalArgumentException();
            for (int i = sources.length - 1; i >= 0; i--){
                prev = current;
                current = new Item<T>(sources[i]);
                current.next = prev;
                if (last == null) last = current;
            }
            last.next = current; // current is first now;
        };

        @Override
        public boolean hasNext() {
            return current.partialIterator.hasNext();
        }

        @Override
        public T next() {
            T result = current.partialIterator.next();
            swapIterators();
            return result;
        }
    }

    @SafeVarargs
    public static <T> Stream<T> zip(Stream<T>... sources) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
                new AlternatingIterator<T>(sources),
                Spliterator.ORDERED | Spliterator.IMMUTABLE), false);
    }

    private static void Exercise_2_8() {
        printHeader();
        zip(characterStream("Hello"), characterStream("world")).forEach(System.out::print);
        System.out.println();
        zip(characterStream("1111111111"), characterStream("222222222222222222"))
                .forEach(System.out::print);
        System.out.println();
        zip(characterStream("——————————————————————————"),
                characterStream("\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"),
                characterStream("||||||||||||||||||"),
                characterStream("//////////////////////////////////"))
                .forEach(System.out::print);
        System.out.println();
    }

    private static boolean isFinite1 (Stream s) {
        throw new RuntimeException("Bad idea"); //no comments
    }

    /**
     * testing stream for finity
     * @param s the examined steram
     * @return true if stream is finite. Else never returns.
     */
    private static boolean isFinite2 (Stream s) {
        s.forEach(it->{});
        return true;
    }

    private static void Exercise_2_7() {
        printHeader();
        System.out.println("Testing finite stream: " + isFinite2(getWarAndPeace()));
        Scanner in = new Scanner(new InputStreamReader(System.in));
        System.out.println("Do you want to test really infinite stream? Type \"Y\" <Enter> for start");
        String s = in.nextLine();
        if ("Y".compareToIgnoreCase(s) == 0) {
            System.out.println("I will work infinitely :(");
            System.out.println("testing finite stream: "
                    + isFinite2(Stream.generate(()->null)));
        }
    }

    public static Stream<Character> characterStream(String s) {
        return IntStream.range(0, s.length())
                .boxed() //because else will "Incompatible types"
                .map(s::charAt);
    }

    private static void Exercise_2_6() {
        printHeader();
        List<String> wordList = Arrays.asList("Hello", "World");
        Stream<String> words = wordList.stream();
        words.flatMap(Chapter2::characterStream).forEach(System.out::println);
    }

    private static void printHeader() {
        // DRY!!! I don't want to write method name mote than two time,
        // but I want to print it out
        String callerName = Thread.currentThread().getStackTrace()[2].getMethodName();
        System.out.printf("\n==={ %s }===================\n", callerName);
    }

    private static Stream<Long> getRandomizer(long a, long c, long m, long seed) {
        return Stream.iterate(seed, (prev)-> (a * prev + c) % m);
    } 
    
    private static void Exercise_2_5() {
        printHeader();
        getRandomizer(25214903917L, 11L, 2L<<48, 12345L)
//                .parallel() // Try to uncomment this :))
                .peek(a -> System.out.println("intermediate: " + a)) // I want to see iterations BEFORE limit
                .limit(5)
                .forEach(a->{
                    System.out.println("RESULT: " + a);
                });
    }

    private static void Exercise_2_4() {
        printHeader();
        int[] values = {1,4,9,16};
        
        System.out.println(Arrays.stream(values).getClass().getName());
        System.out.println(IntStream.of(values).getClass().getName());
    }

    private static Stream<String> getWords(String fileName) {
        try {
            String contents = new String(Files.readAllBytes(Paths.get(fileName)), StandardCharsets.UTF_8);

            return Pattern.compile("[\\P{L}]+").splitAsStream(contents);
        }catch (IOException e) {
            e.printStackTrace();
            return Stream.empty();
        }
    }

    private static Stream<String> getAlice() {
        return getWords("alisa-v-strane-chudes.txt");
    }

    private static Stream<String> getWarAndPeace() {
        return getWords("WarAndPeace.txt");
    }

    private static long timeOf(Runnable action, int repeats) {
        long start, end;
        start = System.currentTimeMillis();
        for (int i = 0; i < repeats; i++) {
            action.run();
        }
        end = System.currentTimeMillis();
        return end - start;
    }

    private static void Exercise_2_3() {
        printHeader();
        System.out.printf(
                "stream: %d milliseconds\n",
                timeOf(()->{
                    getWarAndPeace()
                            .filter(w -> w.length() > 12)
                            .count();

                }, 10)
        );
        System.out.printf(
                "parallelStream: %d milliseconds\n",
                timeOf(()->{
                    getWarAndPeace()
                            .parallel()
                            .filter(w -> w.length() > 12)
                            .count();

                }, 10)
        );
    }

    private static void Exercise_2_2() {
        printHeader();
        long count = getAlice()
                .filter(w -> {
                    boolean result = w.length() > 12;
                    if (result) System.out.println("Found long word: " + w);
                    return result;
                })
                .limit(5)
                .count();
        System.out.println(count);
    }

    private static void Exercise_2_1() {
        printHeader();
        long count = getAlice()
                .filter(w -> w.length() > 12)
                .count();
        System.out.println(count);
    }
}
