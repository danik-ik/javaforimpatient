package Chapter3;

import javafx.scene.paint.Color;

/**
 * Created by danik_ik on 11.08.2017.
 */
@FunctionalInterface
public interface ColorTransformer {
    Color apply(int x, int y, Color colorAtXY);
}
