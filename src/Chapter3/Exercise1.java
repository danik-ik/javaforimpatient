package Chapter3;

import java.util.function.Supplier;
import java.util.logging.Logger;

/**
 * Created by danik_ik on 10.08.2017.
 */
public class Exercise1 {
    static Logger log = Logger.getLogger(Exercise1.class.getName());

    public static void logIf (java.util.logging.Level level, Supplier<String> msg) {
        if (log.isLoggable(level))
            log.log(level, msg.get());
    }
}
