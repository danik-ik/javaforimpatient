package Chapter3;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by danik_ik on 11.08.2017.
 */
public class Exercise2 {
    public static void withLock(ReentrantLock myLock, Runnable action) {
        myLock.lock();
        try {
            action.run();
        } finally {
            myLock.unlock();
        }
    }
}
