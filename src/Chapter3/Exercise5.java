package Chapter3;

import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

public class Exercise5 {
    public static Image transform(Image in, ColorTransformer f) {
        int width = (int)in.getWidth();
        int height = (int)in.getHeight();
        WritableImage out = new WritableImage(width, height);
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                out.getPixelWriter().setColor(x, y,
                        f.apply(x, y, in.getPixelReader().getColor(x,y)));
        return out;
    }
}
