package Chapter3;

import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.util.function.BiFunction;

/**
 * Created by danik_ik on 14.08.2017.
 */
public class Exercise6 {
    public static <T>Image transform(Image in, BiFunction<Color, T, Color> f, T arg) {
        int width = (int)in.getWidth();
        int height = (int)in.getHeight();
        WritableImage out = new WritableImage(width, height);
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                out.getPixelWriter().setColor(x, y,
                        f.apply(in.getPixelReader().getColor(x,y), arg));
        return out;
    }
}
