package Chapter3;

import java.util.Comparator;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

/**
 * Created by danik_ik on 14.08.2017.
 */
public class Exercise7 {

    /**
     *
     * @param ignoreCase: if true, case will be ignored
     * @param ignoreSpaces: if true, will be ignored:
     *                    - spaces before and after the strings;
     *                    - length and kind of spaces between words
     *                    Examples for ignoreSpaces = true:
     *                    " a " == "a"
     *                    "a   a" == "a\ta"
     *                    "aa" != "a a"
     * @return parametrized comparator for strings
     */
    public static Comparator<String> getStringComparator(
            final boolean ignoreCase, final boolean ignoreSpaces){
        UnaryOperator<String> preProc;
        if (ignoreSpaces)
            preProc =
                    (s) -> {
                        s = s.trim();
                        s = s.replaceAll("\\s+", " ");
                        return s;
                    };
        else
            preProc = (s) -> s;

        BiFunction<String, String, Integer> comparingFn;
        if (ignoreCase)
            comparingFn = String::compareToIgnoreCase;
        else
            comparingFn = String::compareTo;

        return (s1, s2) -> {
            s1 = preProc.apply(s1);
            s2 = preProc.apply(s2);
            return comparingFn.apply(s1, s2);
        };
    }
}
