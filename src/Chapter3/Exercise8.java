package Chapter3;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * Task definition: "Generalize Exercise 5 by writing a static method that yields a ColorTransformer
 * that adds a frame of arbitrary thickness and color to an image."
 *
 * difficulties: the ColorTransformer functional interface don't receive
 * any information about width and height of the source image.
 * I can send this information to the factory method, but result will be
 * not applied for any image size!
 *
 * Created by danik_ik on 15.08.2017.
 */
public class Exercise8 {
    /**
     * @param imageWidth
     * @param imageHeight
     * @param borderColor
     * @param borderWidth
     * @return ColorTransformer that creates a frame with specified color and width
     * when applied to image of specified size
     */
    public static ColorTransformer getFrameMaker(double imageWidth, double imageHeight, Color borderColor, int borderWidth) {
        return (x, y, srcColor) -> {
            if (    x < borderWidth
                    || y < borderWidth
                    || y >= imageHeight - borderWidth
                    || x >= imageHeight - borderWidth)
                return borderColor;
            return srcColor;
        };
    }

    /**
     * @param src   image, that define size for frame
     * @param borderColor
     * @param borderWidth
     * @return ColorTransformer that creates a frame with specified color and width
     * when applied to image of specified size
     */
    public static ColorTransformer getFrameMaker(Image src, Color borderColor, int borderWidth) {
        return getFrameMaker(src.getWidth(), src.getHeight(), borderColor, borderWidth);
    }
}
