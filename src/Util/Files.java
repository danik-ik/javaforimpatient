package Util;

import java.io.File;

/**
 * Created by danik_ik on 13.08.2017.
 */
public class Files {
    /**
     * @param fileName
     * @return extention (the end part of the file name, from and include last dot,
     *      where the dot is not at the first position), or "" if extension is absent
     */
    public static String getFileExtension(String fileName) {
        int index = fileName.lastIndexOf('.');
        return index <= 0? "" : fileName.substring(index);
    }

    /**
     * creates list of files with defined extention in the folder
     * @param folderName  the folder for search
     * @param extension   extension for filter. Extension must include a dot
     *                    at the first position or can be ""
     * @return  array of File objects from @folderName folder whth @extension extension
     */
    public static File[] fileListByExtension(String folderName, String extension) {
        File folder = new File(folderName);
        return folder.listFiles(
                (f)->   f.isFile()
                        && getFileExtension(f.getName()).equalsIgnoreCase(extension)
        );
    }

}
