package Util;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by danik_ik on 13.08.2017.
 */
public class Images {
    public static void saveToFile(Image image, String fileName) {
        try {
            File file = new File(fileName);
            RenderedImage renderedImage = SwingFXUtils.fromFXImage(image, null);
            ImageIO.write(
                    renderedImage,
                    "png",
                    file);
        } catch (IOException ex) {
            Logger.getLogger(Images.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Image filledImage(int width, int height, Color color) {
        WritableImage result = new WritableImage(width, height);
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                result.getPixelWriter().setColor(x, y,
                        color);
        return result;
    }

}
