package Chapter3;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by danik_ik on 11.08.2017.
 */

class LogCounter extends java.util.logging.Handler {
    @Override
    public void publish(LogRecord record) { count++; }
    @Override public void flush() {}
    @Override public void close() throws SecurityException {}
    private int count;
    public int getCount() { return count; };
}

@RunWith(Parameterized.class)
public class Exercise1Test {
    private Level level;
    private int lineCount;
    private int actualCount;
    private LogCounter counter = new LogCounter();

    public Exercise1Test(Level level, int lineCount) throws Exception {
        this.level = level;
        this.lineCount = lineCount;
        Exercise1.log.setLevel(level);
        Exercise1.log.addHandler(counter);
    }

    @Parameterized.Parameters(name = "{index}:({0})={1} lines in log")
    public static Iterable<Object[]> dataForTest() {
        return Arrays.asList(new Object[][]{
                {Level.OFF, 0},
                {Level.INFO, 1},
                {Level.FINE, 2},
                {Level.ALL, 3}
        });
    }

    @Test
    public void logIf() throws Exception {
        Level[] levels  = {
                Level.INFO, Level.FINE, Level.ALL
        };
        for (Level l: levels) {
            Exercise1.logIf(l, () -> { actualCount++; return l.getName();});
        }
        assertEquals(lineCount, actualCount);
    }

    @Test
    public void logIfOut() throws Exception {
        Level[] levels  = {
                Level.INFO, Level.FINE, Level.ALL
        };
        for (Level l: levels) {
            Exercise1.logIf(l, () -> l.getName());
        }
        assertEquals(lineCount, counter.getCount());
    }

}