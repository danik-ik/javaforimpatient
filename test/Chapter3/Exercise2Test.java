package Chapter3;

import org.junit.Test;

import java.util.concurrent.locks.ReentrantLock;

import static org.junit.Assert.*;

/**
 * Created by danik_ik on 11.08.2017.
 */
public class Exercise2Test {
    @Test
    public void withLock() throws Exception {
        ReentrantLock l = new ReentrantLock();
        assertFalse(l.isLocked());
        Exercise2.withLock(l,
                () -> { assertTrue(l.isLocked()); });
        assertFalse(l.isLocked());
    }
}