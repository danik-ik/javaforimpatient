package Chapter3;

import Util.Files;
import javafx.scene.image.Image;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;

import static java.lang.String.format;
 
/**
 * Created by danik_ik on 11.08.2017.
 */
@RunWith(Parameterized.class)
public class Exercise5RealImageTest extends RealImageTest {

    @Parameterized.Parameters(name = "{index}: {0})")
    public static Iterable<Object[]> filesForTest() {
        return RealImageTest.filesForTest();
    }

    public Exercise5RealImageTest(File file) {
        super(file);
    }

    @Override
    @Test
    public void border10() throws Exception {super.border10();}
}