package Chapter3;

import Util.Images;
import javafx.scene.paint.Color;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

/**
 * Created by danik_ik on 11.08.2017.
 */
@RunWith(Parameterized.class)
public class Exercise5SimpleTest extends Exercise5Test{

    @Parameterized.Parameters(name = "{index}: ({0}x{1})")
    public static Iterable<Object[]> dataForTest() {
        return Arrays.asList(new Object[][]{
                {10, 10},
                {30, 50},
                {50, 30},
                {320, 240}
        });
    }
    int width;
    int height;

    public Exercise5SimpleTest(int width, int height) {
        super();
        this.width = width;
        this.height = height;
        src = Images.filledImage(width, height, Color.BLACK);
    }

    @Override
    @Test
    public void border10() throws Exception {super.border10();}

}