package Chapter3;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Created by danik_ik on 11.08.2017.
 */
public class Exercise5Test {
    protected static final String testDataDir = "test\\Chapter3\\Data\\";
    protected static final String testOutDir = testDataDir + "out\\";
    private static String filePrefix = Exercise5Test.class.getSimpleName() + ".";
    static {
        // clear old test outputs
        File root = new File(testOutDir);
        if (! root.exists() || ! root.isDirectory())
            throw new RuntimeException("The test output folder is not exists or not a folder");
        for (File myFile : new File(testOutDir).listFiles( (f) -> f.getName().startsWith(filePrefix) ))
            if (myFile.isFile()) myFile.delete();
    }
    public Exercise5Test() {}

    protected Image src;
    protected int borderWidth = 10;
    protected Color borderColor = Color.GRAY;

    protected void test(Image src, int borderWidth, Color borderColor, Image result) {
        // sizes
        assertEquals( "Widths", src.getWidth(), result.getWidth(), 0);
        assertEquals( "heights", src.getHeight(), result.getHeight(), 0);
        // left border
        for (int y = 0; y < src.getHeight(); y++)
            for (int x = 0; x < borderWidth; x++)
                assertEquals(borderColor, result.getPixelReader().getColor(x,y));
        // right border
        for (int y = 0; y < src.getHeight(); y++)
            for (int x = (int)src.getWidth() - borderWidth; x < src.getWidth(); x++)
                assertEquals(borderColor, result.getPixelReader().getColor(x,y));
        // top border
        for (int y = 0; y < borderWidth; y++)
            for (int x = 0; x < src.getWidth(); x++)
                assertEquals(borderColor, result.getPixelReader().getColor(x,y));
        // bottom border
        for (int y = (int)src.getHeight() - borderWidth; y < src.getHeight(); y++)
            for (int x = 0; x < src.getWidth(); x++)
                assertEquals(borderColor, result.getPixelReader().getColor(x,y));
        // picture
        for (int y = borderWidth; y < src.getHeight() - borderWidth; y++)
            for (int x = borderWidth; x < src.getWidth() - borderWidth; x++)
                assertEquals(src.getPixelReader().getColor(x,y), result.getPixelReader().getColor(x,y));
        // For visual control
        Util.Images.saveToFile(result, testOutDir + filePrefix + imageNum++ + ".png");
    }

    protected void border10() throws Exception {
        test(src, borderWidth, borderColor,
                Exercise5.transform(src, (x, y, srcColor) -> {
                    if (    x < borderWidth
                            || y < borderWidth
                            || y >= src.getHeight() - borderWidth
                            || x >= src.getWidth() - borderWidth)
                        return borderColor;
                    return srcColor;
                }));
    }
    private static int imageNum;
}