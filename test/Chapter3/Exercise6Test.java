package Chapter3;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.function.BiPredicate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by danik_ik on 14.08.2017.
 */
@RunWith(Parameterized.class)
public class Exercise6Test {
    private static final String testDataDir = "test\\Chapter3\\Data\\";
    private static final String testOutDir = testDataDir + "out\\";
    private static final String filePrefix = Exercise6Test.class.getSimpleName() + ".";
    private Image src;
    private static int imageNum;
    private static int instanceNum;
    static {
        // clear old test outputs
        for (File myFile : new File(testOutDir).listFiles( (f) -> f.getName().startsWith(filePrefix) ))
            if (myFile.isFile()) myFile.delete();
    }

    @Parameterized.Parameters(name = "{index}: {0})")
    public static Iterable<Object[]> dataForTest() {
        Object[][] result;
        File[] files = Util.Files.fileListByExtension(testDataDir, ".png");
        result = new Object[files.length][1];
        for (int i = 0; i < files.length; i++) result[i][0] = files[i];
        return Arrays.asList(result);
    }

    public Exercise6Test(File file) {
        super();
        try {
            src = new Image(new FileInputStream(file) );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        instanceNum ++;
        imageNum = 0;
    }

    private void saveOut(Image i) {
        String callerName = Thread.currentThread().getStackTrace()[2].getMethodName();
        Util.Images.saveToFile(i, testOutDir + filePrefix + callerName + "." + instanceNum + "." + ++imageNum + ".png");
    }

    private static class Brighterer {
        public static Color process(Color c, Double p) { return c.deriveColor(0, 1, p, 1);};
    }

    @Test
    public void brighter() throws Exception {
        // brighter and brighter
        Image out1 = Exercise6.transform(src, Brighterer::process, 1.2);
        Image out2 = Exercise6.transform(src, Brighterer::process, 1.5);
        saveOut(out1);
        saveOut(out2);
        // brightness test
        testTransform(out1, out2, "second image must be brighter at all (not very bright) pixels",
                (c1, c2) -> c1.getBrightness() > .9 // do not test too bright pixels
                        || c2.getBrightness() > c1.getBrightness());
    }

    @Test
    public void darker() throws Exception {
        // darker and darker
        Image out1 = Exercise6.transform(src, Brighterer::process, .8);
        Image out2 = Exercise6.transform(src, Brighterer::process, .6);
        saveOut(out1);
        saveOut(out2);
        // brightness test
        testTransform(out1, out2, "second image must be darker at all (not very dark) pixels",
                (c1, c2) -> c1.getBrightness() < .1 // do not test too dark pixels
                        || c2.getBrightness() < c1.getBrightness());
    }

    private void testTransform(Image i1, Image i2, String msg, BiPredicate<Color, Color> testFn) {
        assertEquals( "Widths", i1.getWidth(), i2.getWidth(), 0);
        assertEquals( "heights", i1.getHeight(), i2.getHeight(), 0);
        for (int y = 0; y < i1.getHeight(); y++)
            for (int x = 0; x < i1.getWidth(); x++)
                assertTrue(msg, testFn.test(i1.getPixelReader().getColor(x,y), i2.getPixelReader().getColor(x,y)));
    }
}