package Chapter3;

import java.lang.String;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Comparator;

import static org.junit.Assert.*;

/**
 * Created by danik_ik on 14.08.2017.
 */
@RunWith(Parameterized.class)
public class Exercise7Test {
    @Parameterized.Parameters(name = "{index}: («{0}» {4} «{1}»; ignoreCase: {2}; ignoreSpaces: {3})")
    public static Iterable<Object[]> dataForTest() {
        return Arrays.asList(new Object[][]{
                {"aaBB", "AAbb", true, true, "="},
                {"aaBB", "AAbb", false, true, "!="},
                {"aaBB", "AAbb", true, false, "="},
                {"aaBB", "AAbb", false, false, "!="},
                {" aabb ", "aabb", true, true, "="},
                {" aabb ", "aabb", true, false, "!="},
                {" aabb ", "aabb", true, true, "="},
                {" aabb ", "aabb", true, false, "!="},
                {" aabb ", " aa bb ", true, true, "!="},
                {" aabb ", " aa bb ", true, false, "!="},
                {" aa\tbb ", "aa    bb", true, true, "="},
                {" aa\tbb ", "aa    bb", true, false, "!="},
        });
    }

    private String s1;
    private String s2;
    private boolean ignoreCase;
    private boolean ignoreSpaces;
    private String expected;

    public Exercise7Test(String s1, String s2, boolean ignoreCase, boolean ignoreSpaces, String expected) {
        this.s1 = s1;
        this.s2 = s2;
        this.ignoreCase = ignoreCase;
        this.ignoreSpaces = ignoreSpaces;
        this.expected = expected;
    }

    @Test
    public void getStringComparator() throws Exception {
        Comparator<String> comparator = Exercise7.getStringComparator(ignoreCase, ignoreSpaces);
        String result;
        result = comparator.compare(s1, s2) == 0 ? "=" : "!=";
        assertEquals("s1, s2", expected, result);
        result = comparator.compare(s2, s1) == 0 ? "=" : "!=";
        assertEquals("s2, s1", expected, result);
    }
}