package Chapter3;

import javafx.scene.paint.Color;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.util.*;

/**
 * Created by danik_ik on 15.08.2017.
 */
@RunWith(Parameterized.class)
public class Exercise8Test extends RealImageTest{

    private static final Object[][] variants = {
            {13, Color.RED},
            {11, Color.CHOCOLATE},
            {3, Color.AQUAMARINE},
    }; 
    
    @Parameterized.Parameters(name = "{index}: {0}: {1}, {2})")
    public static Iterable<Object[]> dataForTest() {
        List<Object[]> params = new ArrayList<>();
        Iterable<Object[]> files = RealImageTest.filesForTest();
        for (Object[] filesArr: files) 
            for (Object[] otherParams: variants) {
                Object[] arr = new Object[3];
                arr[0] = filesArr[0];
                arr[1] = otherParams[0];
                arr[2] = otherParams[1];
                params.add(arr);
            }
        return params;
    }

    public Exercise8Test(File file, int borderWidth, Color borderColor) {
        super(file);
        this.borderWidth = borderWidth;
        this.borderColor = borderColor;
    }

    @Test
    public void testGetFrameMaker() throws Exception {
        test(src, borderWidth, borderColor,
                Exercise5.transform(src,
                        Exercise8.getFrameMaker(src, borderColor, borderWidth)));
    }

}