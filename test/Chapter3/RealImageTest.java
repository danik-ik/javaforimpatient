package Chapter3;

import javafx.scene.image.Image;
import org.junit.Test;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;

import static java.lang.String.format;

/**
 * Created by admin on 18.09.2017.
 */
public abstract class RealImageTest  extends Exercise5Test {
    public static Iterable<Object[]> filesForTest() {
        Object[][] result;
        File[] files = Util.Files.fileListByExtension(testDataDir, ".png");
        if (files.length == 0) {
            throw new RuntimeException(format("Sample images not found at �%s�", testDataDir));
        }
        result = new Object[files.length][1];
        for (int i = 0; i < files.length; i++) result[i][0] = files[i];
        return Arrays.asList(result);
    }

    public RealImageTest(File file) {
        super();
        try {
            src = new Image(new FileInputStream(file) );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
