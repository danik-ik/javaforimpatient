package Util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by danik_ik on 15.08.2017.
 */
@RunWith(Parameterized.class)
public class FilesTest {
    @Parameterized.Parameters(name = "{index}: (fileName: «{0}»; extension: «{1}»)")
    public static Iterable<Object[]> dataForTest() {
        return Arrays.asList(new Object[][]{
                {".gitignore", ""},
                {".hidden.cfg", ".cfg"},
                {".hidden.1.cfg", ".cfg"},
                {"somefile.txt", ".txt"},
                {"some.other.file.123", ".123"},
                // "somefile." is not tested, because it is wrong file name under Windows
        });
    }
    private String fileName;
    private String expectedExtention;

    public FilesTest(String fileName, String expectedExtention) {
        this.fileName = fileName;
        this.expectedExtention = expectedExtention;
    }

    @Test
    public void getFileExtension() throws Exception {
        assertEquals(expectedExtention, Util.Files.getFileExtension(fileName));
    }

}